import Header from "../../components/header";
interface propsDetail {
  user: object;
}

export default function Detail(props: propsDetail) {
  const { user } = props;
  return (
    <div>
      <Header />
      <div className="cards">
        <h3> {user.title} </h3>
        <span> {user.body} </span>
      </div>
    </div>
  );
}

export async function getStaticPaths() {
  const res = await fetch(`https://jsonplaceholder.typicode.com/posts`);
  const detailUser = await res.json();

  const paths = detailUser.map((item) => ({
    params: {
      id: `${item.id}`,
    },
  }));
  return {
    paths,
    fallback: false,
  };
}
export async function getStaticProps(context) {
  const { id } = context.params;
  const res = await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`);
  const user = await res.json();
  return {
    props: {
      user,
    },
  };
}
