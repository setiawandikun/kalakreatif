import { useRouter } from "next/router";
import Header from "../../components/header";

interface dataProps {
  dataUser: Array<any>;
}

export default function User(props: dataProps) {
  const { dataUser } = props;
  const router = useRouter();
  return (
    <>
      <Header />
      <div className="items">
        {dataUser.splice(0, 10).map(function (item) {
          return (
            <div
              className="cards"
              key={item.id}
              onClick={() => router.push(`/user/${item.id}`)}
            >
              <p>{item.title}</p>
            </div>
          );
        })}
      </div>
    </>
  );
}

export async function getStaticProps() {
  const res = await fetch("https://jsonplaceholder.typicode.com/posts");
  const dataUser = await res.json();
  return {
    props: {
      dataUser,
    },
  };
}
