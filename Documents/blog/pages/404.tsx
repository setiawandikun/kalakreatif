import Link from "next/link";
import { useRouter } from "next/router";

import { useEffect } from "react";

export default function Not404() {
  const router = useRouter();
  useEffect(() => {
    setTimeout(() => {
      router.push("/");
    }, 3000);
  }, []);
  return (
    <div id="notfound">
      <div className="notfound">
        <div className="notfound-404">
          <h1>Oops!</h1>
          <h2>404 - The Page can't be found</h2>
        </div>
        <Link href="/">
          <a>Go TO Homepage</a>
        </Link>
      </div>
    </div>
  );
}
