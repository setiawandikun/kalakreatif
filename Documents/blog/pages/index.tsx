import { useRouter } from "next/router";
import Layout from "../components/layout";
export default function Home() {
  const router = useRouter();
  console.log("home");
  return (
    <Layout>
      <div className="container">
        <h3>M Firman Setiawan </h3>
        <button type="button" onClick={() => router.push("/blog")}>
          Click me
        </button>
      </div>
    </Layout>
  );
}
