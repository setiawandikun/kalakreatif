import { ReactNode } from "react";
import Footer from "../footer";
import Header from "../header";
import style from "./Layout.module.css";

interface PropsChildren {
  children: ReactNode;
}

export default function Layout(props: PropsChildren) {
  const { children } = props;
  return (
    <div className={style.container}>
      <Header />
      <div className={style.conten}>{children}</div>
      <Footer />
    </div>
  );
}
