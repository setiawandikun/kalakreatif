import Link from "next/link";
import style from "./Header.module.css";

export default function Header() {
  return (
    <>
      <div className={style.container}>
        <ul className={style.list}>
          <li className={style.item}>
            <Link href="/">
              <a> Home </a>
            </Link>
          </li>
          <li className={style.item}>
            <Link href="/user">
              <a> Blog </a>
            </Link>
          </li>
          <li className={style.item}>
            <Link href="/other">
              <a> lainya </a>
            </Link>
          </li>
        </ul>
      </div>
    </>
  );
}
