import React from "react";

export const Header = () => {
  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container">
          <a className="navbar-brand" href="/#">
            Daftar Mahasiswa
          </a>
          <div className="ml-auto">
            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarNavAltMarkup"
              aria-controls="navbarNavAltMarkup"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
              <div className="navbar-nav">
                <a className="nav-link active" href="/">
                  Home 
                </a>
                <a className="nav-link" href="/about">
                  Mahasiswa
                </a>
              </div>
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
};
