import axios from "axios";
import React, { useState } from "react";
import { useNavigate } from "react-router";
import { useForm } from "react-hook-form";

function TambahData() {
    const navigate = useNavigate();
    const [post, setPost] = useState({
        name: "M Firman Setiawan",
        npm: 120809,
        prodi: "informatiak"
    })
     const {
       register,
       handleSubmit,
       formState: { errors },
     } = useForm();

    const handleChange = (e, name) => {
       const value = e.target.value
       setPost({...post, [name] : value})
    }
    
    async function onSubmit(e) {
      e.preventDefault();
      try {
        const response = await axios.post("http://localhost:3002/mhs", post);
          setPost(response.data.mhs);
          navigate("/");
      } catch (error) {
        console.error(error);
      }
    }
  return (
    <div>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="form-group">
          <label>Nama</label>
          <input
            type="text"
            className="form-control"
            id="exampleInputEmail1"
            aria-describedby="emailHelp"
            {...register("name", { required: true })}
            placeholder="Masukan Nama"
            onChange={(e) => handleChange(e, "name")}
          />
          {errors.name && (
            <small class="form-text  text-danger">Perlu masukan Nama</small>
          )}
        </div>
        <div className="form-group">
          <label>Npm</label>
          <input
            type="number"
            className="form-control"
            id="exampleInputEmail1"
            placeholder="Masukan Npm"
            {...register("npm", { required: true })}
            onChange={(e) => handleChange(e, "npm")}
          />
          {errors.npm && (
            <small class="form-text  text-danger">Perlu masukan Npm</small>
          )}
        </div>
        <div className="form-group">
          <label>Prodi</label>
          <input
            type="text"
            className="form-control"
            id="exampleInputEmail1 "
            placeholder="Masukan Prodi"
            {...register("prodi", { required: true })}
            onChange={(e) => handleChange(e, "prodi")}
          />
          {errors.prodi && (
            <small class="form-text  text-danger">Perlu masukan prodi</small>
          )}
        </div>

        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </form>
    </div>
  );
}

export default TambahData;
